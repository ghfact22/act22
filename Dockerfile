FROM httpd
MAINTAINER Gerardo Fisanotti
RUN sed -i -e 's/It works/Hola Mundo/' /usr/local/apache2/htdocs/index.html
EXPOSE 8080
ENTRYPOINT httpd -D FOREGROUND 
